<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();

        $schedule->call('App\Http\Controllers\emailValidationController@validated')->hourly();
        $schedule->call('App\Http\Controllers\dataAnalyticController@analyticsPerHour')->hourly();
        $schedule->call('App\Http\Controllers\dataAnalyticController@analyticsPerDay')->hourly();
        $schedule->call('App\Http\Controllers\dataAnalyticController@analyticsCampaignPerHour')->hourly();
        $schedule->call('App\Http\Controllers\dataAnalyticController@analyticsMediumPerHour')->hourly();
        $schedule->call('App\Http\Controllers\dataAnalyticController@analyticsSourcePerHour')->hourly();
        $schedule->call('App\Http\Controllers\dataAnalyticController@analyticsContentPerHour')->hourly();

        $schedule->call('App\Http\Controllers\spamController@spamTemanSobat')->hourly();

        // $schedule->call('App\Http\Controllers\robotController@patroliRejekiSobat')->everyMinute();

        $schedule->call('App\Http\Controllers\dataAnalyticController@insertAnalytic')->everyMinute();

        $schedule->call('App\Http\Controllers\migrationController@baperPoinMigration')->dailyAt('01:28');
        $schedule->call('App\Http\Controllers\DataController@getdata')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\DataController@getDataStw')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\DataController@getdataTeratur')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\DataController@getDataStwTeratur')->everyFiveMinutes();
        $schedule->call('App\Http\Controllers\DataController@temanSobatMigration')->everyFiveMinutes();
        // $schedule->call('App\Http\Controllers\DataController@getdataReseller')->everyMinute();

        $schedule->call('App\Http\Controllers\robotController@patroliTemanSobat')->everyMinute();

        $schedule->call('App\Http\Controllers\ScrapTiktok@updateThumbnail')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
