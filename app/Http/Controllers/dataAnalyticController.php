<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;

class dataAnalyticController extends Controller
{

  public function analyticsPerHour()
  {
    $pullId = DB::table('analytics_hourly')->orderby('id', 'desc')->first();
    $i = $pullId->id + 1;
    $dateAwal = date('2021-07-15 00:00:00');
    $start = date('Y-m-d H:i:s', strtotime($dateAwal . '+ ' . $i . ' hour'));
    $end = date('Y-m-d H:i:s', strtotime($start . '+ 1 hour'));
    if ($start < date('Y-m-d H:i:s')) {
      $count = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->count();
      $clean = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->where('spam', 0)->count();

      $acceleration = $clean / 60;
      if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {
        $save = DB::table('analytics_hourly')->insert([
          'dateTime' => $start,
          'label' => date('d-m [H]', strtotime($start . '+7 hours')),
          'totalData' => $count,
          'cleanData' => $clean,
          'acceleration' => $acceleration
        ]);
      }
    }
  }

  public function analyticsPerDay()
  {
    $pullId = DB::table('analytics_daily')->orderby('id', 'desc')->first();
    $i = $pullId->id + 1;
    $dateAwal = date('2021-04-09 00:00:00');
    $start = date('Y-m-d H:i:s', strtotime($dateAwal . '+ ' . ($i) . ' days'));
    $end = date('Y-m-d H:i:s', strtotime($start . '+ 1 days'));

    $count = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->count();
    $clean = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->where('spam', 0)->count();

    $acceleration = $clean / (60 * 24);


    if ($start < date('Y-m-d H:i:s', strtotime('-1 days'))) {

      $save = DB::table('analytics_daily')->insert([
        'dateTime' => $start,
        'label' => date('d-m', strtotime($start . '+7 hours')),
        'totalData' => $count,
        'cleanData' => $clean,
        'acceleration' => $acceleration
      ]);
      echo "insert " . $start . " " . $clean;
    }
  }

  public function analyticsCampaignPerHour()
  {
    $pullId = DB::table('analytics_campagin')->where('jenisAnalytic', 'timechecker')->orderby('id', 'desc')->first();
    $dateAwal = $pullId->dateTime;
    // /$dateAwal = date('2021-04-10 00:00:00');
    $start = date('Y-m-d H:i:s', strtotime($dateAwal . '+ 1 hour'));
    $end = date('Y-m-d H:i:s', strtotime($start . '+ 2 hour'));


    if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

      $getcampaign = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->distinct()->select('utm_campaign')->get();

      $insert = DB::table('analytics_campagin')->insert([
        'jenisAnalytic' => 'timechecker',
        'namaAnalytics' => 'jumlah campaign',
        'dateTime' => $start,
        'label' => date('d-m [H]', strtotime($start . '+7 hours')),
        'acceleration' => count($getcampaign)
      ]);

      foreach ($getcampaign as $gcamp) {
        $countCampaign = DB::table('users')->where('spam', 0)->where('created_at', '>', $start)->where('created_at', '<', $end)->where('utm_campaign', $gcamp->utm_campaign)->count();

        if (empty($gcamp->utm_campaign)) {
          $gcamp->utm_campaign = 'not defined';
        }

        $countBefore = DB::table('analytics_campagin')->where('namaAnalytics', $gcamp->utm_campaign)->orderby('id', 'desc')->first();
        $lastcount = $countBefore->totalData;

        if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

          $save = DB::table('analytics_campagin')->insert([
            'jenisAnalytic' => 'campaign',
            'namaAnalytics' => $gcamp->utm_campaign,
            'dateTime' => $start,
            'label' => date('d-m [H]', strtotime($start . '+7 hours')),
            'totalData' => $lastcount + $countCampaign,
            'cleanData' => $countCampaign
          ]);
        }
      }
    }
  }

  public function analyticsMediumPerHour()
  {
    $pullId = DB::table('analytics_medium')->where('jenisAnalytic', 'timechecker')->orderby('id', 'desc')->first();
    $dateAwal = $pullId->dateTime;
    //$dateAwal = date('2021-04-10 00:00:00');
    $start = date('Y-m-d H:i:s', strtotime($dateAwal . '+ 1 hour'));
    $end = date('Y-m-d H:i:s', strtotime($start . '+ 2 hour'));


    if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {


      $getmedium = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->distinct()->select('utm_medium')->get();

      $insert = DB::table('analytics_medium')->insert([
        'jenisAnalytic' => 'timechecker',
        'namaAnalytics' => 'jumlah medium',
        'dateTime' => $start,
        'label' => date('d-m [H]', strtotime($start . '+7 hours')),
        'acceleration' => count($getmedium)
      ]);

      foreach ($getmedium as $gmedium) {
        $countmedium = DB::table('users')->where('spam', 0)->where('created_at', '>', $start)->where('created_at', '<', $end)->where('utm_medium', $gmedium->utm_medium)->count();

        if (empty($gmedium->utm_medium)) {
          $gmedium->utm_medium = 'not defined';
        }

        $countBefore = DB::table('analytics_medium')->where('namaAnalytics', $gmedium->utm_medium)->orderby('id', 'desc')->first();
        $lastcount = $countBefore->totalData;

        if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

          $save = DB::table('analytics_medium')->insert([
            'jenisAnalytic' => 'medium',
            'namaAnalytics' => $gmedium->utm_medium,
            'dateTime' => $start,
            'label' => date('d-m [H]', strtotime($start . '+7 hours')),
            'totalData' => $lastcount + $countmedium,
            'cleanData' => $countmedium
          ]);
        }
      }
    }
  }

  public function analyticsSourcePerHour()
  {
    $pullId = DB::table('analytics_source')->where('jenisAnalytic', 'timechecker')->orderby('id', 'desc')->first();
    $dateAwal = $pullId->dateTime;
    //$dateAwal = date('2021-04-10 00:00:00');
    $start = date('Y-m-d H:i:s', strtotime($dateAwal . '+ 1 hour'));
    $end = date('Y-m-d H:i:s', strtotime($start . '+ 2 hour'));


    if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

      $getsource = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->distinct()->select('utm_source')->get();

      $insert = DB::table('analytics_source')->insert([
        'jenisAnalytic' => 'timechecker',
        'namaAnalytics' => 'jumlah source',
        'dateTime' => $start,
        'label' => date('d-m [H]', strtotime($start . '+7 hours')),
        'acceleration' => count($getsource)
      ]);

      foreach ($getsource as $gsource) {
        $countsource = DB::table('users')->where('spam', 0)->where('created_at', '>', $start)->where('created_at', '<', $end)->where('utm_source', $gsource->utm_source)->count();

        if (empty($gsource->utm_source)) {
          $gsource->utm_source = 'not defined';
        }

        $countBefore = DB::table('analytics_source')->where('namaAnalytics', $gsource->utm_source)->orderby('id', 'desc')->first();
        $lastcount = $countBefore->totalData;

        if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

          $save = DB::table('analytics_source')->insert([
            'jenisAnalytic' => 'source',
            'namaAnalytics' => $gsource->utm_source,
            'dateTime' => $start,
            'label' => date('d-m [H]', strtotime($start . '+7 hours')),
            'totalData' => $lastcount + $countsource,
            'cleanData' => $countsource
          ]);
        }
      }
    }
  }

  public function analyticsContentPerHour()
  {
    $pullId = DB::table('analytics_content')->where('jenisAnalytic', 'timechecker')->orderby('id', 'desc')->first();
    $dateAwal = $pullId->dateTime;
    //$dateAwal = date('2021-04-10 00:00:00');
    $start = date('Y-m-d H:i:s', strtotime($dateAwal . '+ 1 hour'));
    $end = date('Y-m-d H:i:s', strtotime($start . '+ 2 hour'));


    if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

      $getcontent = DB::table('users')->where('created_at', '>', $start)->where('created_at', '<', $end)->distinct()->select('utm_content')->get();

      $insert = DB::table('analytics_content')->insert([
        'jenisAnalytic' => 'timechecker',
        'namaAnalytics' => 'jumlah content',
        'dateTime' => $start,
        'label' => date('d-m [H]', strtotime($start . '+7 hours')),
        'acceleration' => count($getcontent)
      ]);


      foreach ($getcontent as $gcont) {
        $countContent = DB::table('users')->where('spam', 0)->where('created_at', '>', $start)->where('created_at', '<', $end)->where('utm_content', $gcont->utm_content)->count();

        if (empty($gcont->utm_content)) {
          $gcont->utm_content = 'not defined';
        }

        $countBefore = DB::table('analytics_content')->where('namaAnalytics', $gcont->utm_content)->orderby('id', 'desc')->first();
        $lastcount = $countBefore->totalData;

        if ($start < date('Y-m-d H:i:s', strtotime('- 1 hours'))) {

          $save = DB::table('analytics_content')->insert([
            'jenisAnalytic' => 'content',
            'namaAnalytics' => $gcont->utm_content,
            'dateTime' => $start,
            'label' => date('d-m [H]', strtotime($start . '+7 hours')),
            'totalData' => $lastcount + $countContent,
            'cleanData' => $countContent
          ]);
        }
      }
    }
  }

  public function perkota()
  {
    $listKota = DB::table('users')->distinct()->select('kota')->get();
    foreach ($listKota as $key => $kotaid) {
      if (!empty($kotaid->kota)) {
        $jmlhperKota = DB::table('users')->where('kota', $kotaid->kota)->count();
        $namaKota = DB::table('regencies')->where('id', $kotaid->kota)->select('name')->first();
        $check_kota = DB::table('analytics_kota')->where('label', $kotaid->kota)->count();

        if ($check_kota < 1) {
          $insert_kota = DB::table('analytics_kota')->insert([
            'jenisAnalytic' => 'kota',
            'namaAnalytics' => $namaKota->name,
            'label' => $kotaid->kota,
            'totalData' => $jmlhperKota
          ]);
        } else {
          $update_kota = DB::table('analytics_kota')->where('label', $kotaid->kota)->update([
            'totalData' => $jmlhperKota
          ]);
        }

        echo $namaKota->name . " -> " . $jmlhperKota . "<br>";
      }
    }
  }

  public function provinsi()
  {
    $listKota = DB::table('users')->distinct()->select('provinsi')->get();
    foreach ($listKota as $key => $kotaid) {
      if (!empty($kotaid->provinsi)) {
        $jmlhperKota = DB::table('users')->where('provinsi', $kotaid->provinsi)->count();
        $namaKota = DB::table('provinces')->where('id', $kotaid->provinsi)->select('name')->first();


        echo $namaKota->name . " -> " . $jmlhperKota . "<br>";
      }
    }
  }

  public function analyticPage(Request $request)
  {
      
    // - Data Register
    // - Email Bersih
    // - WhatsApp Verifikasi
    // - Yang isi alamat
    // - Yang bawa sobat
    // - Login with google
    // - yg main tebak kata
    // a. User lama
    // b. User baru
    
    $total = DB::table('users')->where('created_at', '>=', '2021-07-15')->count();
    $spam = DB::table('users')->where('created_at', '>=', '2021-07-15 00:00:0000')->where('spam', '2')->where('ip_address', '!=', NULL)->count();
    $real = DB::table('users')
      ->where('created_at', '>=', '2021-07-15')
    //   ->where('spam', '!=', '2')
      ->count();
    $emailTrue = DB::table('users')->where('created_at', '>=', '2021-07-15')->where('spam', '!=', 2)->where('email_validation', 'true')->count();

    // dd($real, $emailTrue);
    $thisHour = DB::table('users')->where('created_at', '>', date('Y-m-d H'))->count();
    $minutes = date('i');
    if ($minutes == 0) {
      $minutes = 1;
    }
    session::put('real2', Session::get('real1'));
    session::put('real1', $real);

    $totalAvg = $thisHour;

    $avg = number_format($totalAvg / $minutes, 0, '.', '.');
    $forecast = $avg * 60;

    $percent_spam = $total > 0 ? $spam / $total * 100 : 0;
    $iklan = DB::table('users')->where('id', '<', 10)->get();

    $percentTrue = $real > 0 ?  $emailTrue / $real * 100 : 0;

    // $getcampaign = DB::table('users')->distinct()->select('utm_campaign')->get();

    // foreach ($getcampaign as $gcamp) {
    //   $countCampaign = DB::table('users')
    //     ->where('created_at', '>=', '2021-07-15')
    //     ->where('utm_campaign', $gcamp->utm_campaign)->count();
    //   $campaigns[] = [
    //     'count' => $countCampaign,
    //     'name' => $gcamp->utm_campaign
    //   ];
    // }
    
    $campaigns = DB::table('users')
                 ->where('created_at', '>=', '2021-07-15')
                 ->select('utm_campaign as name', DB::raw('count(*) as count'))
                 ->groupBy('utm_campaign')
                 ->get()->sortByDesc('count');
    // dd($campaigns);

    // $getcontent = DB::table('users')->distinct()->select('utm_content')->get();

    // foreach ($getcontent as $gcont) {
    //   $countContent = DB::table('users')
    //     ->where('created_at', '>=', '2021-07-15')
    //     ->where('utm_content', $gcont->utm_content)->count();
    //   $contents[] = [
    //     'count' => $countContent,
    //     'name' => $gcont->utm_content
    //   ];
    // }
    $contents = DB::table('users')
                 ->where('created_at', '>=', '2021-07-15')
                 ->select('utm_content as name', DB::raw('count(*) as count'))
                 ->groupBy('utm_content')
                 ->get()->sortByDesc('count');;

    // $getmedium = DB::table('users')->distinct()->select('utm_medium')->get();

    // foreach ($getmedium as $gmedium) {
    //   $countmedium = DB::table('users')
    //     ->where('created_at', '>=', '2021-07-15')
    //     ->where('utm_medium', $gmedium->utm_medium)->count();
    //   $mediums[] = [
    //     'count' => $countmedium,
    //     'name' => $gmedium->utm_medium
    //   ];
    // }
    // dd($mediums);
    
    $mediums = DB::table('users')
                 ->where('created_at', '>=', '2021-07-15')
                 ->select('utm_medium as name', DB::raw('count(*) as count'))
                 ->groupBy('utm_medium')
                 ->get()->sortByDesc('count');;

    // $getsource = DB::table('users')->distinct()->select('utm_source')->get();

    // foreach ($getsource as $gsource) {
    //   $countsource = DB::table('users')
    //     ->where('created_at', '>=', '2021-07-15')
    //     ->where('utm_source', $gsource->utm_source)->count();
    //   $sources[] = [
    //     'count' => $countsource,
    //     'name' => $gsource->utm_source
    //   ];
    // }
    
    $sources = DB::table('users')
                 ->where('created_at', '>=', '2021-07-15')
                 ->select('utm_source as name', DB::raw('count(*) as count'))
                 ->groupBy('utm_source')
                 ->get()->sortByDesc('count');;

    // rsort($campaigns);
    // rsort($contents);
    // rsort($sources);
    // rsort($mediums);

    // $data_google = 7419;
    // $data_fb = 2109;
    // $data_ig = 923;
    
     $register = DB::table('users')->where('created_at', '>=', '2021-07-15')->count();
     $emailBersih = DB::table('users')->where('email_validation', 'true')->where('created_at', '>=', '2021-07-15')->count();
     $whatsappVerifikasi = DB::table('users')->where('whatsapp_verification', 1)->where('created_at', '>=', '2021-07-15')->count();
     $alamatRumah = DB::table('users')->where('alamat_rumah', '!=', NULL)->where('created_at', '>=', '2021-07-15')->count();
     $bawaSobat = DB::table('users')->select('ref_id')->where('ref_id', '!=', NULL)->groupBy('ref_id')->where('created_at', '>=', '2021-07-15')->count();
     $loginGoogle = DB::table('users')->where('created_at', '>=', '2021-07-15')->where('providerOrigin', 'Google')->count();
     $tebakKataUserLama = DB::table('jawaban_tebak_kata')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_tebak_kata.user_id')->where('users.providerOrigin', 'Ngabuburit')->get()->count();
     $tebakKataUserBaru = DB::table('jawaban_tebak_kata')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_tebak_kata.user_id')->where('users.providerOrigin', '!=' ,'Ngabuburit')->get()->count();
     $rejekiSobatUserLama = DB::table('jawaban_users')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_users.user_id')->where('users.providerOrigin', 'Ngabuburit')->get()->count();
     $rejekiSobatUserBaru = DB::table('jawaban_users')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_users.user_id')->where('users.providerOrigin', '!=' ,'Ngabuburit')->get()->count();
     
    return view('pages.analytic', compact('mediums', 'sources', 'contents', 'campaigns', 'total', 'iklan', 'percent_spam', 'spam', 'real', 'campaigns', 'contents', 'sources', 'mediums', 'avg', 'totalAvg', 'forecast', 'emailTrue', 'percentTrue', 'register', 'emailBersih', 'whatsappVerifikasi', 'alamatRumah', 'bawaSobat', 'loginGoogle', 'tebakKataUserLama', 'tebakKataUserBaru', 'rejekiSobatUserLama', 'rejekiSobatUserBaru'));
  }
  
  public function dataTambahan(){
      $register = DB::table('users')->where('created_at', '>=', '2021-07-15')->count();
      $emailBersih = DB::table('users')->where('email_validation', 'true')->where('created_at', '>=', '2021-07-15')->count();
      $whatsappVerifikasi = DB::table('users')->where('whatsapp_verification', 1)->where('created_at', '>=', '2021-07-15')->count();
      $alamatRumah = DB::table('users')->where('alamat_rumah', '!=', NULL)->where('created_at', '>=', '2021-07-15')->count();
      $bawaSobat = DB::table('users')->select('ref_id')->where('ref_id', '!=', NULL)->groupBy('ref_id')->where('created_at', '>=', '2021-07-15')->count();
      $loginGoogle = DB::table('users')->where('created_at', '>=', '2021-07-15')->where('providerOrigin', 'Google')->count();
      $tebakKataUserLama = DB::table('jawaban_tebak_kata')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_tebak_kata.user_id')->where('users.providerOrigin', 'Ngabuburit')->get()->count();
      $tebakKataUserBaru = DB::table('jawaban_tebak_kata')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_tebak_kata.user_id')->where('users.providerOrigin', '!=' ,'Ngabuburit')->get()->count();
      $rejekiSobatUserLama = DB::table('jawaban_users')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_users.user_id')->where('users.providerOrigin', 'Ngabuburit')->get()->count();
      $rejekiSobatUserBaru = DB::table('jawaban_users')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_users.user_id')->where('users.providerOrigin', '!=' ,'Ngabuburit')->get()->count();
      
      
      echo "Register = ". $register."<br>";
      echo "Email Bersih = ". $emailBersih."<br>";
      echo "Whatsapp Verifikasi = ". $whatsappVerifikasi."<br>";
      echo "Isi Alamat Rumah = ". $alamatRumah."<br>";
      echo "Yang bawa sobat = ". $bawaSobat."<br>";
      echo "Login Google = ". $loginGoogle."<br>";
      echo "Tebak Kata User Lama = ". $tebakKataUserLama."<br>";
      echo "Tebak Kata User Baru = ". $tebakKataUserBaru."<br>";
      echo "Rejeki Sobat User Lama = ". $rejekiSobatUserLama."<br>";
      echo "Rejeki Sobat User Baru = ". $rejekiSobatUserBaru."<br>";
  }
  
  public function insertAnalytic(){
      date_default_timezone_set('Asia/Jakarta');
         $register = DB::table('users')->where('created_at', '>=', '2021-07-15')->count();
         $emailBersih = DB::table('users')->where('email_validation', 'true')->where('created_at', '>=', '2021-07-15')->count();
         $whatsappVerifikasi = DB::table('users')->where('whatsapp_verification', 1)->where('created_at', '>=', '2021-07-15')->count();
         $alamatRumah = DB::table('users')->where('alamat_rumah', '!=', NULL)->where('created_at', '>=', '2021-07-15')->count();
         $bawaSobat = DB::table('users')->select('ref_id')->where('ref_id', '!=', NULL)->groupBy('ref_id')->where('created_at', '>=', '2021-07-15')->count();
         $loginGoogle = DB::table('users')->where('created_at', '>=', '2021-07-15')->where('providerOrigin', 'Google')->count();
         $tebakKataUserLama = DB::table('jawaban_tebak_kata')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_tebak_kata.user_id')->where('users.providerOrigin', 'Ngabuburit')->get()->count();
         $tebakKataUserBaru = DB::table('jawaban_tebak_kata')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_tebak_kata.user_id')->where('users.providerOrigin', '!=' ,'Ngabuburit')->get()->count();
         $rejekiSobatUserLama = DB::table('jawaban_users')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_users.user_id')->where('users.providerOrigin', 'Ngabuburit')->get()->count();
         $rejekiSobatUserBaru = DB::table('jawaban_users')->select('users.id')->distinct()->join('users', 'users.id', 'jawaban_users.user_id')->where('users.providerOrigin', '!=' ,'Ngabuburit')->get()->count();
      if(DB::table('analytics')->count() == 0){
         DB::table('analytics')->insert([
             'register' => $register,
             'email_bersih' => $emailBersih,
             'whatsapp_verifikasi' => $whatsappVerifikasi,
             'alamat_rumah' => $alamatRumah,
             'login_google' => $loginGoogle,
             'tebak_kata_lama' => $tebakKataUserLama,
             'tebak_kata_baru' => $tebakKataUserBaru,
             'rejeki_sobat_baru' => $rejekiSobatUserBaru,
             'rejeki_sobat_lama' => $rejekiSobatUserLama,
             'created_at' => date('Y-m-d H:i:s'),
             'updated_at' => date('Y-m-d H:i:s')
             ]);
     }
     else{
         DB::table('analytics')->truncate();
         DB::table('analytics')->insert([
             'register' => $register,
             'email_bersih' => $emailBersih,
             'whatsapp_verifikasi' => $whatsappVerifikasi,
             'alamat_rumah' => $alamatRumah,
             'login_google' => $loginGoogle,
             'tebak_kata_lama' => $tebakKataUserLama,
             'tebak_kata_baru' => $tebakKataUserBaru,
             'rejeki_sobat_baru' => $rejekiSobatUserBaru,
             'rejeki_sobat_lama' => $rejekiSobatUserLama,
             'created_at' => date('Y-m-d H:i:s'),
             'updated_at' => date('Y-m-d H:i:s')
             ]);
     }
     echo "Success"."<br>";
  }
  
     public function temanSobatMigration(){
            echo "start";
            $pulls = DB::table('users')->where('migration_ts_status', 0)->take(2000)->get();
            foreach($pulls as $pull){
                $count = DB::table('users')->where('ref_id', $pull->id)->count();
                DB::table('users')->where('id', $pull->id)->update([
                    'countRef' => $count,
                    'migration_ts_status' => 1
                    ]);
                echo $pull->email.'<br>';
            }
            echo "Success";
    }
  
}
