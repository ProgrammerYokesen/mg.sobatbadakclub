<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use cookie;
use Mailgun\Mailgun;
use App\User;
use Illuminate\Support\Str;
use DB;

class emailWebinar extends Controller
{
    private $emailAccount = '';
    private $from = 'Sobat Badak <support@sobatbadak.club>';
    private $subject = 'Registrasi Webinar';
    private $template = '';
    
    
    public function validateMail(){
        $client = new Client();
        $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$this->emailAccount.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
        $res = json_decode($result->getBody());
        $check = $res->mailbox_verification;
        DB::connection('webinar')->table('users')->where('email', $this->emailAccount)->update([
            'email_validation' => $check
            ]);
        return $check;
    }
    
    public function registerWebinar(Request $request){
            $this->emailAccount = $request->email;
            $user = DB::connection('webinar')->table('users')->where('email', $this->emailAccount)->first();
            $webinar = DB::connection('webinar')->table('webinars')->where('id', $user->webinar_id)->first();
            $var = '{"$name": "' . $user->name . '","$title": "' . $webinar->hero_title . '","$date": "' . $webinar->time_event . '","$jam": "' . $webinar->time_event . '"}';
            if($this->validateMail() == 'true'){
                
                /**Send mail disini */
                $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
                
                $params = array(
                    'from'    => $this->from,
                    'to'      => $this->emailAccount,
                    'subject' => $this->subject,
                    'template'    => 'webinar-register',
                    'v:title'   => $webinar->hero_title,
                    'v:name' => $user->name,
                    'v:date' => $webinar->time_event,
                    'v:jam' => $webinar->time_event,
                    'h:X-Mailgun-Variables'    => '{"$name": "' . $user->name . '","$title": "' . $webinar->hero_title . '","$date": "' . $webinar->time_event . '","$jam": "' . $webinar->time_event . '"}'
                  );
                //  return $params;
                $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'),$params);
                return response()->json([
                    'Status' => "Success",
                    'Message' => "Email reset password telah terkirim"
                ]);
            }
            else{
                /**Mail cannot be validated */
                return response()->json([
                    'Status' => "Failed",
                    'Message' => '1'
                ]);
            }
    }
    
}
