<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use cookie;
use Mailgun\Mailgun;
use App\User;
use Illuminate\Support\Str;
use DB;

class forgotPasswordMail extends Controller
{
    private $emailAccount = '';
    private $from = 'Sobat Badak <support@sobatbadak.club>';
    private $subject = 'Forgot Password';
    private $template = 'forgot-password';
    private $url = 'https://sobatbadak.club/reset-password?token=';
    
    public function createUserToken(){
        $client = new Client();
        $code = md5(date('Y-m-d H:i:s').$this->emailAccount).time();
        $user = DB::table('users')->where('email', $this->emailAccount)->first();
        if($user){
            DB::table('users')->where('email', $this->emailAccount)->update([
            'forgot_password_token' => $code
            ]);
            return $code;
        }
        else{
            return 'Failed';
        }
    }
    
    public function validateMail(){
        $client = new Client();
        $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$this->emailAccount.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
        $res = json_decode($result->getBody());
        $check = $res->mailbox_verification;
        return $check;
    }
    
    public function forgotPassword(Request $request){
            $this->emailAccount = $request->email;
            if($this->validateMail() == 'true'){
                $token = $this->createUserToken();
                if($token == 'Failed'){
                  return response()->json([
                    'Status' => 'Failed',
                    'Message' => '2'
                ]);
                }
                $link = $this->url.$token;
                /**Send mail disini */
                $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
                $params = array(
                    'from'    => $this->from,
                    'to'      => $this->emailAccount,
                    'subject' => $this->subject,
                    'template'    => $this->template,
                    'v:link'   => $link,
                    'h:X-Mailgun-Variables'    => '{"$link": "'.$link.'"}'
                  );
                $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'),$params);
                return response()->json([
                    'Status' => "Success",
                    'Message' => "Email reset password telah terkirim"
                ]);
            }
            else{
                /**Mail cannot be validated */
                return response()->json([
                    'Status' => "Failed",
                    'Message' => '1'
                ]);
            }
    }
    
    public function verifikasiEmail(Request $request){
            $this->emailAccount = $request->email;
            $this->subject = "Verifikasi Email";
            $this->template = "email-verifikasi";
            $this->url = "https://sobatbadak.club/verifikasi-email?token=";
            if($this->validateMail() == 'true'){
                $token = $this->createVerifyToken();
                if($token == 'Failed'){
                  return response()->json([
                    'Status' => 'Failed',
                    'Message' => '2'
                ]);
                }
                $link = $this->url.$token;
                /**Send mail disini */
                $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
                $params = array(
                    'from'    => $this->from,
                    'to'      => $this->emailAccount,
                    'subject' => $this->subject,
                    'template'    => $this->template,
                    'v:link'   => $link,
                    'h:X-Mailgun-Variables'    => '{"$link": "'.$link.'"}'
                  );
                $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'),$params);
                return response()->json([
                    'Status' => "Success",
                    'Message' => "Email reset password telah terkirim"
                ]);
            }
            else{
                /**Mail cannot be validated */
                return response()->json([
                    'Status' => "Failed",
                    'Message' => '1'
                ]);
            }
    }
    
    public function createVerifyToken(){
           $client = new Client();
        $code = md5(date('Y-m-d H:i:s').$this->emailAccount).time();
        $user = DB::table('users')->where('email', $this->emailAccount)->first();
        if($user){
            DB::table('users')->where('email', $this->emailAccount)->update([
            'token_verify_email' => $code
            ]);
            return $code;
        }
        else{
            return 'Failed';
        }
    }
    
}
