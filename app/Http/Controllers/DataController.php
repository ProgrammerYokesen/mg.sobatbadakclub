<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client;

class DataController extends Controller
{
     public function getdata(){
         $dateNow = date('Y-m-d');
         $dateTime = $dateNow.' 15:00:00';
         $endTime = $dateNow.' 21:00:00';
         $currentTime = date('Y-m-d H:i:s');
        // $startTime = \DB::connection('csb')->table('event_time')->where('time','<=',$dateTime)->where('end_time', '>', $dateTime)->first();
        // $startTimeEvent = $startTime->time;
        
        if($currentTime <= $endTime){
            $clientGetData = new Client();
            $resGetData = $clientGetData->get("https://gudang.warisangajahmada.com/api/get-order/$dateTime")->getBody();
            $responseGetData = json_decode($resGetData);
            $data = $responseGetData->data;
            
            foreach($data as $key=>$value){
                $check = \DB::table('orders')->where('invoice', $value->Invoice)->count();
                // dd($check);
                    if($check < 1){
                         $insert = \DB::table('orders')->insertGetId([
                        'invoice'=> $value->Invoice,
                        'nama'=>$value->nmPembeli,
                        'email'=>$value->email,
                        'noHp'=>$value->noHp,
                        'platform'=>$value->platform,
                        'qtyTotal'=>0,
                        'created_at'=>$value->created_at,
                        // 'time_event_id'=>$startTime->id
                        ]);
                    // insert detail
                        $qty = 0;
                        $sku ='';
                        foreach($value->order as $eachOrder){
                            // dd($eachOrder);
                            $sku = $eachOrder->sku;
                            $qty = $qty + $eachOrder->qty;
                            $insertDetail = \DB::table('order_detail')->insert([
                              'order_id'=>$insert,
                              'nama_item'=>$eachOrder->item,
                              'qty'=> $eachOrder->qty,
                              'product_id'=>$eachOrder->product_id,
                              'created_at'=>$value->created_at,
                              'sku'=>$eachOrder->sku,
                            ]);
                        }
                        $update = \DB::table('orders')->where('invoice', $value->Invoice)->update([
                         'qtyTotal'=>$qty,
                         'sku'=>$sku
                        ]);
                        $data[$key]->qtyTotal = $qty;
                        
                    }
            }
            // dd($data);
            // return response()->json($resGetData);
        }
        
        
    }
    
    public function getDataStw(){
        // $dateNow = date('Y-m-d');
        $clientGetData = new Client();
        $resGetData = $clientGetData->get("https://gudang.warisangajahmada.com/api/get-order-stw")->getBody();
        $responseGetData = json_decode($resGetData);
        $data = $responseGetData->data;
        
        foreach($data as $key=>$value){
                $check = \DB::table('orders')->where('invoice', $value->Invoice)->count();
                // dd($check);
                    if($check < 1){
                         $insert = \DB::table('orders')->insertGetId([
                        'invoice'=> $value->Invoice,
                        'nama'=>$value->nmPembeli,
                        'email'=>$value->email,
                        'noHp'=>$value->noHp,
                        'platform'=>$value->platform,
                        'qtyTotal'=>0,
                        'created_at'=>$value->created_at,
                        // 'time_event_id'=>$startTime->id
                        ]);
                    // insert detail
                        $qty = 0;
                        $sku ='';
                        foreach($value->order as $eachOrder){
                            // dd($eachOrder);
                            $sku = $eachOrder->sku;
                            $qty = $qty + $eachOrder->qty;
                            $insertDetail = \DB::table('order_detail')->insert([
                              'order_id'=>$insert,
                              'nama_item'=>$eachOrder->item,
                              'qty'=> $eachOrder->qty,
                              'product_id'=>$eachOrder->product_id,
                              'created_at'=>$value->created_at,
                              'sku'=>$eachOrder->sku,
                            ]);
                        }
                        $update = \DB::table('orders')->where('invoice', $value->Invoice)->update([
                         'qtyTotal'=>$qty,
                         'sku'=>$sku
                        ]);
                        $data[$key]->qtyTotal = $qty;
                        
                    }
            }
            // dd($data);
            echo 'sukses';
            // return response()->json($resGetData);
    }
    // public function getDataReseller(){
    //     // $dateNow = date('Y-m-d');
    //     $clientGetData = new Client();
    //     $resGetData = $clientGetData->get("https://gudang.warisangajahmada.com/api/get-order-reseller")->getBody();
    //     $responseGetData = json_decode($resGetData);
    //     $data = $responseGetData->data;
        
    //     foreach($data as $key=>$value){
    //             $check = \DB::table('orders')->where('invoice', $value->Invoice)->count();
    //             // dd($check);
    //                 if($check < 1){
    //                      $insert = \DB::table('orders')->insertGetId([
    //                     'invoice'=> $value->Invoice,
    //                     'nama'=>$value->nmPembeli,
    //                     'email'=>$value->email,
    //                     'noHp'=>$value->noHp,
    //                     'platform'=>$value->platform,
    //                     'qtyTotal'=>0,
    //                     'created_at'=>$value->created_at,
    //                     // 'time_event_id'=>$startTime->id
    //                     ]);
    //                 // insert detail
    //                     $qty = 0;
    //                     $sku ='';
    //                     foreach($value->order as $eachOrder){
    //                         // dd($eachOrder);
    //                         $sku = $eachOrder->sku;
    //                         $qty = $qty + $eachOrder->qty;
    //                         $insertDetail = \DB::table('order_detail')->insert([
    //                           'order_id'=>$insert,
    //                           'nama_item'=>$eachOrder->item,
    //                           'qty'=> $eachOrder->qty,
    //                           'product_id'=>$eachOrder->product_id,
    //                           'created_at'=>$value->created_at,
    //                           'sku'=>$eachOrder->sku,
    //                         ]);
    //                     }
    //                     $update = \DB::table('orders')->where('invoice', $value->Invoice)->update([
    //                      'qtyTotal'=>$qty,
    //                      'sku'=>$sku
    //                     ]);
    //                     $data[$key]->qtyTotal = $qty;
                        
    //                 }
    //         }
    //         // dd($data);
    //         echo 'sukses';
    //         // return response()->json($resGetData);
    // }
     public function getdataTeratur(){
         $dateNow = date('Y-m-d');
         $dateTime = $dateNow.' 15:00:00';
         $endTime = $dateNow.' 21:00:00';
         $currentTime = date('Y-m-d H:i:s');
        // $startTime = \DB::connection('csb')->table('event_time')->where('time','<=',$dateTime)->where('end_time', '>', $dateTime)->first();
        // $startTimeEvent = $startTime->time;
        
        if($currentTime <= $endTime){
            $clientGetData = new Client();
            $resGetData = $clientGetData->get("https://teratur.warisangajahmada.com/api/get-order/$dateTime")->getBody();
            $responseGetData = json_decode($resGetData);
            $data = $responseGetData->data;
            foreach($data as $key=>$value){
                $check = \DB::table('orders')->where('invoice', $value->Invoice)->count();
                // dd($check);
                    if($check < 1){
                         $insert = \DB::table('orders')->insertGetId([
                        'invoice'=> $value->kode,
                        'nama'=>$value->nama_pemesan,
                        'email'=>$value->email,
                        'noHp'=>$value->phone,
                        'platform'=>$value->platform,
                        'qtyTotal'=>0,
                        'created_at'=>$value->created_at,
                        // 'time_event_id'=>$startTime->id
                        ]);
                    // insert detail
                        $qty = 0;
                        $sku ='';
                        foreach($value->order as $eachOrder){
                            // dd($eachOrder);
                            $sku = $eachOrder->sku;
                            $qty = $qty + $eachOrder->qty;
                            $insertDetail = \DB::table('order_detail')->insert([
                              'order_id'=>$insert,
                              'nama_item'=>$eachOrder->item,
                              'qty'=> $eachOrder->qty,
                              'product_id'=>$eachOrder->product_id,
                              'created_at'=>$value->created_at,
                              'sku'=>$eachOrder->sku,
                            ]);
                        }
                        $update = \DB::table('orders')->where('invoice', $value->Invoice)->update([
                         'qtyTotal'=>$qty,
                         'sku'=>$sku
                        ]);
                        $data[$key]->qtyTotal = $qty;
                        
                    }
            }
            // dd($data);
            // return response()->json($resGetData);
        }
        
        
    }
    
    public function getDataStwTeratur(){
        // $dateNow = date('Y-m-d');
        $clientGetData = new Client();
        $resGetData = $clientGetData->get("https://teratur.warisangajahmada.com/api/get-order-stw")->getBody();
        $responseGetData = json_decode($resGetData);
        $data = $responseGetData->data;
        foreach($data as $key=>$value){
                $check = \DB::table('orders')->where('invoice', $value->Invoice)->count();
                // dd($check);
                    if($check < 1){
                         $insert = \DB::table('orders')->insertGetId([
                        'invoice'=> $value->kode,
                        'nama'=>$value->nama_pemesan,
                        'email'=>$value->email,
                        'noHp'=>$value->phone,
                        'platform'=>$value->platform,
                        'qtyTotal'=>0,
                        'created_at'=>$value->created_at,
                        // 'time_event_id'=>$startTime->id
                        ]);
                    // insert detail
                        $qty = 0;
                        $sku ='';
                        foreach($value->order as $eachOrder){
                            // dd($eachOrder);
                            $sku = $eachOrder->sku;
                            $qty = $qty + $eachOrder->qty;
                            $insertDetail = \DB::table('order_detail')->insert([
                              'order_id'=>$insert,
                              'nama_item'=>$eachOrder->item,
                              'qty'=> $eachOrder->qty,
                              'product_id'=>$eachOrder->product_id,
                              'created_at'=>$value->created_at,
                              'sku'=>$eachOrder->sku,
                            ]);
                        }
                        $update = \DB::table('orders')->where('invoice', $value->Invoice)->update([
                         'qtyTotal'=>$qty,
                         'sku'=>$sku
                        ]);
                        $data[$key]->qtyTotal = $qty;
                        
                    }
            }
            // dd($data);
            echo 'sukses';
            // return response()->json($resGetData);
    }
    
    public function getDataPerDay(){
        $now = date('Y-m-d');
        $getDataText = \DB::table('orders')->whereDate('created_at', $now)->select('nama', 'id', 'invoice')->get();
        $qtyTotal =0;
        foreach($getDataText as $key=>$value){
            $getDetail = \DB::table('order_detail')->where('order_id', $value->id)->get();
            $qtyTotal = 0;
            foreach($getDetail as $index=>$item){
                // $getDataText[$key]->order[$index] = [
                // 'item'=>$item->nama_item,
                // 'qty'=>$item->qty,
                // 'sku'=>$item->sku
                // ];
                $qtyTotal = $qtyTotal + $item->qty;
            }
            $getDataText[$key]->qtyTotal = $qtyTotal;
            unset($getDataText[$key]->id);
        }
        // dd($getDataText);
        // return response()->json($getDataText);
    }
}
