<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Auth;

class migrationController extends Controller
{
    public function baperPoinMigration(){
        $pulls = DB::table('user_poins')->groupBy('user_poins.user_id')
                ->select(DB::raw('sum(user_poins.poin) as sum'), 'users.email', 'users.name', 'users.id')
                ->where('user_poins.status', 1)
                ->where('users.migration_bp_status', 0)
                ->join('users', 'users.id', 'user_poins.user_id')
                ->get();
        foreach($pulls as $pull){
            $bp = DB::table('users')->where('id', $pull->id)->first()->baper_poin;
            $newBP = $bp + $pull->sum;
            DB::table('users')->where('id', $pull->id)->update([
                'migration_bp_status' => 1,
                'baper_poin' => $newBP
                ]);
        }
        
    }
    
    public function temanSobatMigration(){
            $pulls = DB::table('users')->where('migration_ts_status', 0)->take(3000)->get();
            foreach($pulls as $pull){
                $count = DB::table('users')->where('ref_id', $pull->id)->count();
                DB::table('users')->where('id', $pull->id)->update([
                    'countRef' => $count,
                    'migration_ts_status' => 1
                    ]);
                echo $pull->email.'<br>';
            }
            echo "Success";
    }

    
}
