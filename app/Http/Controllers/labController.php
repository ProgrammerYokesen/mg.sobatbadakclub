<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class labController extends Controller
{
    public function todayPoin(){
            date_default_timezone_set('Asia/Jakarta');
            $date = date('Y-m-d H:is');
            $maxPoin = DB::table('quiz_rejeki_sobat')
            ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
            ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
            ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
            ->where('quiz_rejeki_sobat.status', 1)
            ->where('quiz_rejeki_sobat.start_date', '<', $date)
            ->where('quiz_rejeki_sobat.end_date', '>', $date)
            ->sum('poin');
            $fkId = DB::table('quiz_rejeki_sobat')
                ->where('quiz_rejeki_sobat.status', 1)
                ->where('quiz_rejeki_sobat.start_date', '<', $date)
                ->where('quiz_rejeki_sobat.end_date', '>', $date)
                ->first();
            dd($maxPoin, $fkId);
    }

    public function countRefCheck(){
        $datas = DB::table('users')->select('id')->get();
    }

    public function errorCheck(){
        abort(406);
    }

    public function leaderboardReferral($id){
      $data = DB::table('leaderboards_referral')->join('users', 'users.id', 'leaderboards_referral.user_id')->where('leaderboards_referral.status', 1)->where('referral_events', $id)->orderByDesc('amount')->take(20)->get();
      foreach ($data as $key => $value) {
        // code...
        echo $value->name." - ", $value->whatsapp."<br>";
      }
    }
    
    public function countTemanSobat()
    {
        $users = DB::table('users')->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->where('statistic_process',0)->take(5000)->get();
        foreach($users as $user)
        {
            $verifiedTs = DB::table('users')->where('ref_id', $user->id)->where('whatsapp_verification', 1)->where('emailValidation', 'validated')->count();
            $unverified =  DB::table('users')->where('ref_id', $user->id)->where(function($query)
                {
                    $query->where('whatsapp_verification','!=', 1)
                    ->orWhere('emailValidation','!=', 'validated');
                })->count();
            DB::table('user_statistics')->updateOrInsert([
                'user_id' => $user->id
                ],[
                    'verified_ts' => $verifiedTs,
                    'notverified_ts' => $unverified
                    ]);
            DB::table('users')->where('id', $user->id)->update([
                'statistic_process' => 1
                ]);
        }
        return 'success';
    }

}
