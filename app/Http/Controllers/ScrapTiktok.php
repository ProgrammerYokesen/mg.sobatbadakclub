<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;

class ScrapTiktok extends Controller
{
  public function updateThumbnail()
  {
    $dateNow = now();
    $now =strtotime($dateNow);
    $now = $now + 1000;
    $data = DB::table('sppa_videos')->where(function ($query) use ($now) {
              $query->where('thumbnail_expired', '<', $now);
          })->get();
    foreach($data as $k => $v)
    {
        try {
            $client = new Client();
            $response = $client->request('GET', 'https://www.tiktok.com/oembed?url='.$v->original_link);
            $res = $response->getBody()->getContents();
            $responses = json_decode($res);
            // dd($responses);
            $url = $responses->thumbnail_url;
            $parts = parse_url($url);
            parse_str($parts['query'], $query);
            // print_r($query["x-expires"]);
            // dd($responses->html, $url, $query["x-expires"], $v->id);
            DB::table('sppa_videos')->where('id', $v->id)->update([
              'thumbnail' => $url,
              'thumbnail_expired' => $query["x-expires"],
              'updated_at' => now()
            ]);
        // dd($responses);
      } catch (\Exception $e) {

      }
    }
  }
}
