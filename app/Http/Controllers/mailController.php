<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use cookie;
use Mailgun\Mailgun;
use App\User;
use Illuminate\Support\Str;

require '../vendor/autoload.php';


class mailController extends Controller
{
    /**
     * First change your credentials on .env
     * @from fill with your email
     * @subject fill with your email's subject
     * @template fill with your template's name on mailgun dashboard
     * @var string
     */
    private $emailAccount = '';
    private $from = 'Sobat Badak <support@sobatbadak.club>';
    private $subject = 'uji coba';
    private $template = 'test-email';

    /**
     * Email reset password
     */
    public function sendMail(Request $req){
        $this->emailAccount = $req->email;

            if($this->validateMail() == 'true'){
                $token = $this->createUserToken();
                if($token == 'Failed'){
                  return response()->json([
                    'Status' => 'Failed',
                    'Message' => 'User not found'
                ]);
                }
                /**Send mail disini */
                $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.mailgun.net'); // For EU servers
                $params = array(
                    'from'    => $this->from,
                    'to'      => $this->emailAccount,
                    'subject' => $this->subject,
                    'template'    => $this->template,
                    'v:token'   => $token,
                    'h:X-Mailgun-Variables'    => '{"$token": "'.$token.'"}'
                  );
                $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'),$params);
                return response()->json([
                    'Status' => "Success",
                    'Message' => "Email reset password telah terkirim"
                ]);
            }
            else{
                /**Mail cannot be validated */
                return response()->json([
                    'Status' => "Failed",
                    'Message' => 'Alamat email gagal di validasi'
                ]);
            }
    }
    
  public function forgotPassword(Request $request){
      $code = $request->token;
    $client = new Client();
    $result = $client->get(env('API_BASE_URL').'get_lost_password_by_code?code='.$code, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $res = json_decode($result->getBody());
    $this->emailAccount = $res->email;
    $email = $res->email;

    $client = new Client();
    $user_get = $client->get(env('API_BASE_URL').'search_user_by_email?email='.$email, [
      'headers' => [
        'X-Authorization-Token' => md5(env('API_KEY')),
        'X-Authorization-Time'  => time()
      ]
    ]);

    $user = json_decode($user_get->getBody());
    $name = $user->name;

    /**
     * seeing the detail where row=$id
     */ 
    $rules = array(
      'password' => 'required|confirmed:password_confirmation'
    );

    $validator = \Validator::make($request->all(), $rules);
    if ($validator->fails())
    {
      return redirect()->route('profileIndex')->withInput()->withErrors($validator);
    }
    else
    {
      $client = new Client();
      $change_password = $client->post(env('API_BASE_URL').'change_password', [
        'headers' => [
          'X-Authorization-Token' => md5(env('API_KEY')),
          'X-Authorization-Time'  => time()
        ],
        'form_params' => [
          'id' => $user->id,
          'email' => $user->email,
          'password' => $request->password
        ]
      ]);
      $change = json_decode($change_password->getBody());
      return response()->json([
        'Status' => 'Success',
        'Message' => 'Your password has been changed'
      ]);
    }
  }

/**
 * Check email using mailgun
 * Exist or not
 * @return void
 */
    public function validateMail(){
        $client = new Client();
        $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$this->emailAccount.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
        $res = json_decode($result->getBody());
        $check = $res->mailbox_verification;
        return $check;
    }
    
    public function createUserToken(){
        $client = new Client();
        $code = md5(date('Y-m-d H:i:s').$this->emailAccount).time();
    
        $res = $client->post('https://api.traderindo.com/api/send-token-forgot-password', [
          'form_params' => [
            'email' => $this->emailAccount,
          ]
        ]);
        $res = json_decode($res->getBody());
        if($res->Status == 'Success'){
          return $code;
        }
        else{
          return 'Failed';
        }
    }


}
