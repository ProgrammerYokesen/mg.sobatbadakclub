<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class spamController extends Controller
{

    public function spamTemanSobat(){
        $total = 0;
        $satujamlalu = date('Y-m-d H:i:s',strtotime('-7 minutes'));
              //$satujamlalu = date('2021-04-10 00:00:00');
              $ref = DB::table('users')->where('ref_id','>',9)->where('created_at','>',$satujamlalu)->distinct()->select('ref_id')->get();
              //dd($satujamlalu,$ref);
              foreach ($ref as $value) {
                  $getref = DB::table('users')->where('ref_id',$value->ref_id)->count();
                  //$check_spam = DB::table('users')->where('id',$value->ref_id)->first();
                  if($getref > 24){
                      $ipdistinct = DB::table('users')->where('ref_id',$value->ref_id)->distinct()->select('ip_address')->get();
                      $ipcount = count($ipdistinct);
                      $selisih = $getref - $ipcount;
                      $ratio = $selisih/$getref *100;
                      if($ratio>50){
                        $total += $getref;
                        echo $value->ref_id." --> ".$getref." --> ".$ipcount." --> ".$selisih." --> [".number_format($ratio,2,'.','.')." % same IP Address ]<br>";
                        $update = DB::table('users')->where('id',$value->ref_id)->update([
                          'spam' => 1
                        ]);
                        $update = DB::table('users')->where('ref_id',$value->ref_id)->update([
                          'spam' => 2
                        ]);
                      }
                  }
              }
              $totaluser = DB::table('users')->count();
              $percentuser = $total/$totaluser*100;
              echo "\n";
              echo "-------------------------------------\nCount Spam data : ".$total."\n";
              echo "Total Data Register : ".$totaluser."\n";
              echo "Percent spam from total data : ".number_format($percentuser,2,'.','.')." %";
    }

    public function spamTemanSobatAll(){
      $total = 0;
      $satujamlalu = '2021-10-21 22:06:20';
            //$satujamlalu = date('2021-04-10 00:00:00');
            $ref = DB::table('users')->where('ref_id',223120)->where('spam', 0)->distinct()->select('ref_id')->get();
            foreach ($ref as $value) {
                $getref = DB::table('users')->where('ref_id',$value->ref_id)->count();
                //$check_spam = DB::table('users')->where('id',$value->ref_id)->first();
                if($getref > 24){
                    $ipdistinct = DB::table('users')->where('ref_id',$value->ref_id)->distinct()->select('ip_address')->get();
                    $ipcount = count($ipdistinct);
                    $selisih = $getref - $ipcount;
                    $ratio = $selisih/$getref *100;
                    if($ratio>50){
                      $total += $getref;
                      echo $value->ref_id." --> ".$getref." --> ".$ipcount." --> ".$selisih." --> [".number_format($ratio,2,'.','.')." % same IP Address ]<br>";
                      $update = DB::table('users')->where('id',$value->ref_id)->update([
                        'spam' => 1
                      ]);
                      $update = DB::table('users')->where('ref_id',$value->ref_id)->update([
                        'spam' => 2
                      ]);
                    }
                }
            }
            $totaluser = DB::table('users')->count();
            $percentuser = $total/$totaluser*100;
            echo "\n";
            echo "-------------------------------------\nCount Spam data : ".$total."\n";
            echo "Total Data Register : ".$totaluser."\n";
            echo "Percent spam from total data : ".number_format($percentuser,2,'.','.')." %";
    }

    public function temanSobatLeaderboard(){
      $date = date('Y-m-d H:i:s');
      $activeEvent = DB::table('referral_events')
      ->where('referral_events.start_time', '<', $date)
      ->where('referral_events.end_time', '>', $date)
      ->first();
      if($activeEvent){
          $data = DB::table('leaderboards_referral')->join('users', 'users.id', 'leaderboards_referral.user_id')->where('leaderboards_referral.status', 1)->where('referral_events', $activeEvent->id)->orderByDesc('amount')->take(20)->get();
      }
      $output = $data->where('spam', '!=', 0);
      dd($output);
    }


}
