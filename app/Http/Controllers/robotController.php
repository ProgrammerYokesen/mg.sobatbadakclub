<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class robotController extends Controller
{

    public function patroliRejekiSobat(){
        date_default_timezone_set('Asia/Jakarta');
        $date = date('Y-m-d H:is');
        $maxPoin = DB::table('quiz_rejeki_sobat')
        ->select('quiz_rejeki_sobat.id as quizId', 'pertanyaan_rejeki_sobat.id as pertanyaan_id', 'pertanyaan_rejeki_sobat.pertanyaan', 'pertanyaan_rejeki_sobat.jawaban_a', 'pertanyaan_rejeki_sobat.jawaban_b', 'pertanyaan_rejeki_sobat.jawaban_c', 'pertanyaan_rejeki_sobat.jawaban_d')
        ->leftJoin('quiz_rejeki_sobat_list', 'quiz_rejeki_sobat_list.quiz_rejeki_sobat_id', 'quiz_rejeki_sobat.id')
        ->leftJoin('pertanyaan_rejeki_sobat', 'quiz_rejeki_sobat_list.pertanyaan_id', 'pertanyaan_rejeki_sobat.id')
        ->where('quiz_rejeki_sobat.status', 1)
        ->where('quiz_rejeki_sobat.start_date', '<', $date)
        ->where('quiz_rejeki_sobat.end_date', '>', $date)
        ->sum('poin');
        $fkId = DB::table('quiz_rejeki_sobat')
                ->where('quiz_rejeki_sobat.status', 1)
                ->where('quiz_rejeki_sobat.start_date', '<', $date)
                ->where('quiz_rejeki_sobat.end_date', '>', $date)
                ->first();
        if($fkId){
            $pulls = DB::table('user_poins')
                            ->where('from', 'quiz rejeki sobat')
                            ->where('robot_check', 0)
                            ->where('fk_id', $fkId->id)
                            ->limit(50)
                            ->get();
            foreach($pulls as $pull){
                echo $pull->id."\n";
                if($pull->poin > $maxPoin){
                    DB::table('user_poins')->where('id', $pull->id)->update([
                        'status' => 0,
                        'robot_check' => 1
                        ]);

                    //Change baper poin in users
                    $baperPoin = DB::table('users')->where('id', $pull->user_id)->first()->baper_poin;
                    $bp = $baperPoin - $pull->poin;
                    DB::table('users')->where('id', $winner->user_id)->update([
                        'baper_poin' => $bp
                    ]);
                }
                else{
                      DB::table('user_poins')->where('id', $pull->id)->update([
                        'robot_check' => 1
                        ]);
                }
            }
        }
    }

    public function patroliTemanSobat(){
      $pullSpammer = DB::table('users')->where('spam', 1)->get();
      foreach($pullSpammer as $key => $value){
        // $get = DB::table('users')->where('ref_id', $value->id)->get();
        // foreach ($get as $k => $v) {
          // code...
          $leaderBoard = DB::table('leaderboards_referral')->where('user_id', $value->id)->first();
          if($leaderBoard){
            $spamTotal = $leaderBoard->amount;
            $count = DB::table('user_poins')->where('user_id', $value->id)->where('from', 'Bawa teman sobat')->where('poin', 50000)->where('status', 0)->count();
            $total = $spamTotal - $count;
            if($spamTotal > 24){
              for ($i=0; $i < $total; $i++) {
                // code...
                DB::table('user_poins')->where('user_id', $value->id)->where('from', 'Bawa teman sobat')->where('poin', 50000)->update([
                  'status' => 0
                ]);

                /** update on user table */
                $baperPoin = DB::table('users')->where('id', $value->id)->first()->baper_poin;
                $newBp = $baperPoin - 50000;
                DB::table('users')->where('id', $value->id)->update([
                  'baper_poin' => $newBp
                ]);
              }
            }
          }
        // }
      }
    }

}
