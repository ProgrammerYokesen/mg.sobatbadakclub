<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/lab/today-poin', 'labController@todayPoin');
Route::get('/teratur/getData', 'DataController@getdataTeratur');
Route::get('/teratur/getData/stw', 'DataController@getDataStwTeratur');

Route::get('/lab/baper-poin', 'migrationController@baperPoinMigration');
Route::get('/lab/teman-sobat-migration', 'migrationController@temanSobatMigration');
Route::get('erorr', 'labController@errorCheck');

Route::get('lab/check-spam-teman-sobat', 'spamController@spamTemanSobat');
Route::get('lab/spam-teman-sobat', 'spamController@spamTemanSobatAll');
Route::get('lab/teman-sobat-leaderboard', 'spamController@temanSobatLeaderboard');
Route::get('lab/spam-sobat-bp', 'robotController@patroliTemanSobat');
Route::get('admin/referral_events/{id}', 'labController@leaderboardReferral');
Route::get('lab/teman-sobat-statistic', 'labController@countTemanSobat');
Route::get('tiktok-thumbnail', 'ScrapTiktok@updateThumbnail');
